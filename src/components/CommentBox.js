import React, { useState } from 'react';
import { useDispatch } from 'react-redux';

import * as actions from '../store/actions/index';

const commentBox = () => {
    const [comment, setComment] = useState('');

    const dispatch = useDispatch();
    const onFetchComment = () => dispatch(actions.fetchComments());
    const onSaveComment = (comment) => dispatch(actions.saveComment(comment));

    const handleSubmit = (event) => {
        event.preventDefault();
        onSaveComment(comment);
        setComment('');
    }

    return (
        <div>
            <form onSubmit={handleSubmit}>
                <h4>Add a Comment</h4>
                <textarea
                    value={comment}
                    onChange={(event) => setComment(event.target.value)}
                />
                <div>
                    <button>Submit Comment</button>
                </div>
            </form>
            <button className="fetch-comments" onClick={onFetchComment}>Fetch Comments</button>
        </div>

    );
}

export default commentBox;