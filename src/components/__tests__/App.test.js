import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';

import App from '../App';
import CommentBox from '../CommentBox';
import CommentList from '../CommentList';

Enzyme.configure({ adapter: new Adapter() });

let wrapper;

beforeEach(() => {
    wrapper = shallow(<App />);
});

it('shows a comment box', () => {
    expect(wrapper.find(CommentBox).length).toEqual(1);
});

it('shows a comment list', () => {
    expect(wrapper.find(CommentList).length).toEqual(1);
});