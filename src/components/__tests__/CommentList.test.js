import React from 'react';
import Enzyme, { mount } from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';

import Root from '../../Root';
import CommentList from '../CommentList';

Enzyme.configure({ adapter: new Adapter() });

let wrapper;

beforeEach(() => {
    const initialState = {
        comment: { data: ['Comment 1', 'Comment 2'] }
    };

    wrapper = mount(
        <Root initialState={initialState}>
            <CommentList />
        </Root>
    );
});

afterEach(() => {
    wrapper.unmount();
});

it('creates one LI per comment', () => {
    expect(wrapper.find('li').length).toEqual(2);
});

it('shows the text for each comment', () => {
    expect(wrapper.render().text()).toContain('Comment 1');
    expect(wrapper.render().text()).toContain('Comment 2');
});