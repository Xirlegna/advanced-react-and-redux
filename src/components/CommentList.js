import React from 'react';
import { useSelector } from 'react-redux';

const commentList = () => {
    const comment = useSelector(state => state.comment);

    const renderComments = () => {
        return comment.data.map((item) => (<li key={Math.random()}>{item}</li>));
    };

    return (
        <div>
            <ul>
                {renderComments()}
            </ul>
        </div>
    );
}

export default commentList;