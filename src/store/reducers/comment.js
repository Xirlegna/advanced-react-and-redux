import * as actionTypes from '../actions/actionTypes';

const initialState = {
    data: []
};

const fetchCommentsStart = (state) => {
    return {
        ...state
    };
};

const fetchCommentsSuccess = (state, action) => {
    return {
        ...state,
        data: action.data.map((comment) => comment.body)
    };
};

const saveComment = (state, action) => {
    return {
        ...state,
        data: [...state.data, action.data]
    };
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.FETCH_COMMENTS_START: return fetchCommentsStart(state);
        case actionTypes.FETCH_COMMENTS_SUCCESS: return fetchCommentsSuccess(state, action);
        case actionTypes.SAVE_COMMENT: return saveComment(state, action);
        default: return state;
    }
};

export default reducer;