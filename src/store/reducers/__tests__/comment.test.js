import commentReducer from '../comment';
import * as actionTypes from '../../actions/actionTypes';

it('handles actions of type SAVE_COMMENT', () => {
    const action = {
        type: actionTypes.SAVE_COMMENT,
        data: 'New Comment'
    };

    const newState = commentReducer({ data: [] }, action);

    expect(newState).toEqual({ data: ['New Comment'] });
});

it('handles action with unknown type', () => {
    const action = {
        type: 'WRONG_ACTION'
    };

    const newState = commentReducer([], action);

    expect(newState).toEqual([]);
});