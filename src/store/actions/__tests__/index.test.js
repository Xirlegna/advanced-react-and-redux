import { saveComment } from '../comment';
import * as actionTypes from '../actionTypes';

describe('saveComment', () => {
    it('has the correct type', () => {
        const action = saveComment();

        expect(action.type).toEqual(actionTypes.SAVE_COMMENT);
    });

    it('has the correct payload', () => {
        const action = saveComment('New Comment');

        expect(action.data).toEqual('New Comment');
    });
});