import * as actionTypes from './actionTypes';
import axios from 'axios';

export const fetchCommentsStart = () => {
    return {
        type: actionTypes.FETCH_COMMENTS_START
    }
};

export const fetchCommentsSuccess = (comments) => {
    return {
        type: actionTypes.FETCH_COMMENTS_SUCCESS,
        data: comments
    }
};

export const fetchComments = () => {
    return dispatch => {
        dispatch(fetchCommentsStart());
        axios.get('https://jsonplaceholder.typicode.com/comments')
            .then(response => {
                dispatch(fetchCommentsSuccess(response.data));
            })
            .catch(error => {
                console.log(error);
            });
    };
};

export const saveComment = (comment) => {
    return {
        type: actionTypes.SAVE_COMMENT,
        data: comment
    }
};