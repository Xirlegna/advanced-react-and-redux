import React from 'react';
import { createStore, applyMiddleware, compose, combineReducers } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';

import comment from './store/reducers/comment';

const root = ({ children, initialState = {} }) => {
    const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
    const rootReducer = combineReducers({ comment });
    const store = createStore(rootReducer, initialState, composeEnhancers(
        applyMiddleware(thunk)
    ));

    return (
        <Provider store={store}>
            {children}
        </Provider>
    );
};

export default root;